import JSONDATA from './MOCK_DATA.json';
import './App.css';
import {useState} from 'react';

function App() {
  return (
    <div className="App">
      <div className="container"></div>
      add new player: <input className="input" />
      <button>submit</button> {"\n"} <br></br>
      edit player: <input className="edit" />
      <button>submit</button> {"\n"}<br></br> 
      <br></br>
      <br></br>
      <input className="search" type="text" placeholder="search..." onChange/>
      <button>submit</button>
      {JSONDATA.map((val, key) => {
        return (
        <div> 
          <p>{val.username}</p> 
          </div>
          );
      })}
    </div>
  );
}

export default App;
